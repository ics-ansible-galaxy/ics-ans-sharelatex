ics-ans-sharelatex
==================

Ansible playbook to install ShareLatex.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

License
-------

BSD 2-clause
