import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('sharelatex')


def test_sharelatex_index(host):
    # This tests that traefik forwards traffic to ShareLatex
    # and that we can access the ShareLatex index page
    cmd = host.run('curl -H Host:sharelatex.docker.localhost -k -L https://localhost')
    assert 'ESS ShareLaTeX, the Online LaTeX Editor' in cmd.stdout
